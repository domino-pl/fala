# Fala

Program na zaliczeniu przedmiotu Programowanie Obiektowe - java.

Wizualizacja i emulacja dźwięku w wyniku złożenia dwóch fal - dudnień. 


[Link do specyfikacji projektu](https://docs.google.com/document/d/1YA6zr_EUV8cux0uiPyDkPwMyqEHpvlk0xdpWx0pW77Q/edit?usp=sharing)

## Zmiany:

D2:
- W stosunku do D1, w D2 rezygnujemy z możliwości zapisania własnego dźwięku do bazy. Możliwy będzie wybór predefiniowanego dźwięku z listy rozwijanej. Ponadto dodajemy regulację głośności odtwarzania i opcję wyciszenia.
- Dodaliśmy propozycję punktacji.

---