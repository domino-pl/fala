package fala;

import java.awt.event.ActionListener;

//import java.math.RoundingMode;
//import java.text.DecimalFormat;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/** 
 * Klasa reprezenująca pole tekstowe do wprowadzania i wyświetlania częstotliwości.
 * Do poprawnej pracy wymaga podpięcia dokładnie jednego actionListener ze względu na budowę metody ustawHz.
 **/
@SuppressWarnings("serial")
public class PoleTekstoweF extends JTextField {
	float minF;  /** minimalna możliwa do ustawienia częstotliwość. **/
	float maxF;  /** maksymalna możliwa do ustawienia częstotliwość. **/
	
	/** Konstruktor ustawiający minimalną i maksymalną możliwe częstotliwości. **/
	public PoleTekstoweF(float minF, float maxF) {
		super();
		this.minF = minF;
		this.maxF = maxF;
		
		this.setHorizontalAlignment(SwingConstants.RIGHT);
	}
	
	/** 
	 * Pobiera z pola tekstowe częstosliwość w Hz.
	 * W wypadku nie powodzenia zwraca -1.
    **/
	public synchronized float pobierzHz() {
		try {
			String text = this.getText().trim().toLowerCase();
			
			if(text.endsWith("hz"))
				text = text.substring(0, text.length()-2).trim();
			
			float hz = (float) (Math.round(Float.parseFloat(text) * 10) / 10.0);
			
			return (this.minF <= hz && hz <= this.maxF) ? hz : -1;
		}
		catch(NumberFormatException e) {
			return -1;
		}
	}
	
	/** Wyświetla w polu tekstowym żądaną częstotliwość. **/
	public void ustawHz (float hz) {
		SwingUtilities.invokeLater(
			new Runnable(){
				public void run() {
					ActionListener actionListener = getActionListeners()[0];
					removeActionListener(actionListener);
					setText(""+hz+" Hz ");
					addActionListener(actionListener);
				}
			}
		);
	}	
}