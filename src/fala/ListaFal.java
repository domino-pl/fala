package fala;

import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.SwingUtilities;

/** Klasa repezentująca listę predefiniowanych częstotliwości fal. **/
@SuppressWarnings("serial")
public class ListaFal extends JComboBox<String> {
	Map <Float, String> tony;  /** Predefinowane fale. **/
	float wartoscHz;  /** Aktualnie ustawiona warotść. **/
	
	/** Konstruktor **/
	public ListaFal() {
		super();
		
		tony = new HashMap<Float, String>();
		
		/** Predefiniowanie fal **/
		tony.put((float) -1, "Inna");
		tony.put((float) 440, "440.0 Hz - a1");
		tony.put((float) 441, "441.0 Hz - a1+1Hz");
		tony.put((float) 370, "370.0 Hz - fis1");
		
		/** Dodanie fal do elementów listy. **/
		for (Map.Entry<Float,String> entry : tony.entrySet())  
            this.addItem(entry.getValue());
	}
	
	/** 
	 * Ustawienie żądaną częstotliwość.
	 * Jeżeli nie ma takiej w liście wybiera opcje "Inna". 
	**/
	public synchronized void ustawHz(float hz) {
		SwingUtilities.invokeLater(
			new Runnable(){
				public void run() {
					ActionListener actionListener = getActionListeners()[0];
					removeActionListener(actionListener);
					setSelectedItem(
						tony.containsKey(hz) ? tony.get(hz) : tony.get((float)-1)
					);
					addActionListener(actionListener);
				}
			}
		);
	}
	
	/** 
	 * Pobiera wartość częstotliwości w Hz.
	 * Jeżeli wybrano "Inna" zwraca -1.
	**/
	public float pobierzHz() {
		for (Map.Entry<Float,String> entry : tony.entrySet())  
            if(entry.getValue().equals(this.getSelectedItem())) {
            	return entry.getKey();
            }
		return -1;
	}
		
}
	
	
