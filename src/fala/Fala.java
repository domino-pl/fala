package fala;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

/** Główna klasa programu. **/
@SuppressWarnings("serial")
public class Fala extends JFrame {
	/** SEKCJA KONFIGURACYJNA **/
	float configMinF = 15;  /** minimalna możliwa do ustawienia częstotliwość **/
	float configMaxF = 25000;  /** minimalna możliwa do ustawienia częstotliwość **/
	int configZapisDlugosc = 30;  /** Czas trawania zapisywanego pliku **/
	int configCzęstotliwoscAnimacji = 40; /** Częstotliwość odświerzania animacji wykresów. **/
	int configRysowanyCzasC = 10; /** Czas rysowany na wyjściu funkcji C, **/
	
    /** Główne panele układu programu **/
	//OsCzasu osCzasu;
    Wykres rysunekC;
    Wykres rysunekB;
    Wykres rysunekA;   
    PanelInfo panelWyniki;
    JPanel panelLogo;
    JPanel panelOdtwarzanie;
    JPanel panelOpcjeB;
    JPanel panelOpcjeA;
   
    /** Kontrolki wyboru źródła na panelu odtwarzania. **/
    JRadioButton wyborZrodloA;
    JRadioButton wyborZrodloB;
    JRadioButton wyborZrodloAB;
    /** Przyciski na panelu odtwarzania. **/
    JButton przyciskOdtwarzaj;
    JButton przyciskWstrzymaj;
    JButton przyciskZapisz;
    /** Ustawienie zapętlenia. **/
    JCheckBox przelacznikZapetlenia;
    /** Ustawienie głośności. **/
    JToggleButton przyciskWyciszenie;
    JSlider suwakGlosnosci;
    
    /** Opcje ustawień funkcji źródłowych. **/
    SuwakF suwakCzestotliwoscA;
    SuwakF suwakCzestotliwoscB;
    PoleTekstoweF poleTekstoweCzestotliwoscA;
    PoleTekstoweF poleTekstoweCzestotliwoscB;
    ListaFal listaCzestotliwoscA;
    ListaFal listaCzestotliwoscB;
   
    /** Dzwięki, generator, sterowenik, odtwarzacz, malarz. **/
    DzwiekSinus dA, dB;
    DzwiekZlozenie dC;
    Generator generator;
    Odtwarzacz odtwarzacz;
    Sterownik sterownik;
    Malarz malarz;
    
    private void initPaneli(boolean colorDebug) {
		this.setLayout(new GridBagLayout());
		
	//	osCzasu = new OsCzasu();

		rysunekC = new Wykres();
		rysunekB = new Wykres();
		rysunekA = new Wykres();
	
		panelWyniki = new PanelInfo();
	    panelLogo = new JPanel();
	    panelOdtwarzanie = new JPanel();
	    panelOpcjeB = new JPanel();
	    panelOpcjeA = new JPanel();
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(2,2,2,2); 
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.weightx = 100; // automatycznie maksymalne możliwe rozszerzenie kolumny 1.
		c.weighty = 0; // komórka osi czasu ma mieć możliwie małą wysokość
	//	this.add(osCzasu, c);
		
		c.fill = GridBagConstraints.BOTH;
		c.gridy = 1;
		c.gridheight = 3;
		c.weighty = 0.5; // rysynek C ma być 2x większy niż rysunki A i B
		this.add(rysunekC, c);
		
		c.gridy = 4;
		c.gridheight = 1;
		c.weighty = 0.25;
		this.add(rysunekB, c);		
		
		c.gridy = 5;     
		c.weighty = 0.25;
		this.add(rysunekA, c);				
		
		c.gridx = 1;       
		c.gridy = 0;       
		c.gridheight = 2;
		c.weightx = 0; // kolumna 2. ma mieć minimalną szerokość (wynikającą z komponentów wewnątrz
		c.weighty = 0; // elementy w kolumenie 2. mają dostosowywać wysokość to tej wynikającej z kolumny 1.
		this.add(panelWyniki, c);	
		
		c.gridx = 1;       
		c.gridy = 2;       
		c.gridheight = 1;
		this.add(panelLogo, c);	
		
		c.gridy = 3;      
		c.gridheight = 1;
		this.add(panelOdtwarzanie, c);
		
		c.gridy = 4;      
		this.add(panelOpcjeB, c);
		
		c.anchor = GridBagConstraints.LINE_END;
		c.gridy = 5;      
		this.add(panelOpcjeA, c);
		
		if(colorDebug) {
		    Map<JPanel, Color> mapa = new HashMap<JPanel,Color>();  
		    
		 //   mapa.put(osCzasu, Color.GREEN);
		    mapa.put(rysunekC, Color.ORANGE);
		    mapa.put(rysunekB, Color.BLUE);
		    mapa.put(rysunekA, Color.CYAN);
		    mapa.put(panelWyniki, Color.lightGray);
		    mapa.put(panelLogo, Color.RED);
		    mapa.put(panelOdtwarzanie, Color.GRAY);
		    mapa.put(panelOpcjeB, Color.YELLOW);
		    mapa.put(panelOpcjeA, Color.PINK);
		    
		    for(Map.Entry<JPanel, Color> m:mapa.entrySet()){
		    	m.getKey().setBackground(m.getValue());
		    	m.getKey().setBorder(BorderFactory.createLineBorder(Color.black));
		    }
		}
    }
     
    /** Tworzenie panelu z logiem **/
    private void ladowaniePaneluLoga() {
    	panelLogo.setBorder(BorderFactory.createLineBorder(Color.black, 4, true));
		
    	panelLogo.setLayout(new BoxLayout(panelLogo, BoxLayout.Y_AXIS));
		
		JLabel etykietaLogo = new JLabel();
		JLabel etykietaDP = new JLabel("Dominik Przybysz (298125)", JLabel.CENTER);
		JLabel etykietaBK = new JLabel("Bartosz Kowalski (298111)", JLabel.CENTER);
		
		try {
			BufferedImage icon;
			icon = ImageIO.read( ClassLoader.getSystemResource( "img/logo_wfpw.png" ) );
			etykietaLogo.setIcon(new ImageIcon(icon));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		etykietaLogo.setAlignmentX(Component.CENTER_ALIGNMENT);
		etykietaDP.setAlignmentX(Component.CENTER_ALIGNMENT);
		etykietaBK.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		Border marginesObrazka = BorderFactory.createEmptyBorder(10, 10, 12, 10);
		Border marginesNazwisk = BorderFactory.createEmptyBorder(0, 10, 5, 10);
		etykietaLogo.setBorder(marginesObrazka);
		etykietaDP.setBorder(marginesNazwisk);
		etykietaBK.setBorder(marginesNazwisk);
		
		panelLogo.add(etykietaLogo);
		panelLogo.add(etykietaDP);
		panelLogo.add(etykietaBK);
    }
    
    /** Ładowanie paneli z opcjami konfiguacymi fali A i B. **/
    private void ladowaniePaneliOpcjiWejsc() {
    	panelOpcjeA.setBorder(BorderFactory.createLineBorder(Color.black, 4, true));
    	panelOpcjeB.setBorder(BorderFactory.createLineBorder(Color.black, 4, true));
    	
    	suwakCzestotliwoscA = new SuwakF(this.configMinF, this.configMaxF);
    	suwakCzestotliwoscB = new SuwakF(this.configMinF, this.configMaxF);
    	poleTekstoweCzestotliwoscA = new PoleTekstoweF(this.configMinF, this.configMaxF);
    	poleTekstoweCzestotliwoscB = new PoleTekstoweF(this.configMinF, this.configMaxF);
    	listaCzestotliwoscA = new ListaFal();
    	listaCzestotliwoscB = new ListaFal();

    	panelOpcjeA.setLayout(new GridBagLayout());
    	panelOpcjeB.setLayout(new GridBagLayout());
			
		GridBagConstraints c = new GridBagConstraints();
		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(2,5,2,5); // marginesy 
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		panelOpcjeA.add(new JLabel("Fala A:"), c);
		panelOpcjeB.add(new JLabel("Fala B:"), c);
		
		c.gridy = 1;
		panelOpcjeA.add(poleTekstoweCzestotliwoscA, c);
		panelOpcjeB.add(poleTekstoweCzestotliwoscB, c);

		c.gridy = 2;
		panelOpcjeA.add(suwakCzestotliwoscA, c);
		panelOpcjeB.add(suwakCzestotliwoscB, c);
		
		c.gridy = 3;
		panelOpcjeA.add(listaCzestotliwoscA, c);
		panelOpcjeB.add(listaCzestotliwoscB, c);
 
		// wypełnienie odsuwające pozostałe komponenty do góry panelu
		c.gridy = 4;
		c.weighty = 1;
		panelOpcjeA.add(Box.createHorizontalGlue(), c);
		panelOpcjeB.add(Box.createHorizontalGlue(), c);
 
    }
    
    /** Ładowanie panelu sterującego odtawrzaniem. **/
    private void ladowaniePaneluOdtwarzania() {
    	panelOdtwarzanie.setBorder(BorderFactory.createLineBorder(Color.black, 4, true));
		
    	wyborZrodloA = new JRadioButton("A");
	    wyborZrodloB = new JRadioButton("B");
	    wyborZrodloAB = new JRadioButton("A+B=C", true);
	    
	    ButtonGroup group = new ButtonGroup();
		group.add(wyborZrodloA);
		group.add(wyborZrodloB);
		group.add(wyborZrodloAB);
	 	
		przelacznikZapetlenia =  new JCheckBox("Zapętlenie");
	    
	    przyciskOdtwarzaj = new JButton();
	    przyciskWstrzymaj = new JButton();
	    przyciskZapisz = new JButton();
	    przyciskWyciszenie = new JToggleButton();

		BufferedImage icon;
	    try {
			icon = ImageIO.read( ClassLoader.getSystemResource( "img/odtwarzanie.png" ) );
			przyciskOdtwarzaj.setIcon(new ImageIcon(icon));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    try {
			icon = ImageIO.read( ClassLoader.getSystemResource( "img/pauza.png" ) );
			przyciskWstrzymaj.setIcon(new ImageIcon(icon));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    try {
			icon = ImageIO.read( ClassLoader.getSystemResource( "img/wav.png" ) );
			przyciskZapisz.setIcon(new ImageIcon(icon));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    try {
			icon = ImageIO.read( ClassLoader.getSystemResource( "img/glosnosc.png" ) );
			przyciskWyciszenie.setIcon(new ImageIcon(icon));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    przyciskWstrzymaj.setEnabled(false);

	    
	    suwakGlosnosci =  new JSlider(0, 125, 100);
	    
	    
	    // Dzięki temu nie jest wymuszana szerokość przycisku większa niż 30px (tyle ma ikona)
	    // Ustawienie to zachowuje się tu jak setMinimumSize, więc dałem 32px szerokości dla ikon 30x30px
	    przyciskOdtwarzaj.setPreferredSize(new Dimension(32, przyciskOdtwarzaj.getPreferredSize().height));
	    przyciskWstrzymaj.setPreferredSize(new Dimension(32, przyciskWstrzymaj.getPreferredSize().height));
	    przyciskZapisz.setPreferredSize(new Dimension(32, przyciskZapisz.getPreferredSize().height));
	    // To ustawienie analogicznie
	    suwakGlosnosci.setPreferredSize(new Dimension(100, suwakGlosnosci.getPreferredSize().height));
		
	    
	    panelOdtwarzanie.setLayout(new GridBagLayout());
		GridBagConstraints o = new GridBagConstraints();
		
		o.fill = GridBagConstraints.BOTH;
		o.insets = new Insets(1,1,1,1); 
		o.gridx=0;
		o.gridy=0;
		panelOdtwarzanie.add(wyborZrodloAB, o);
		
		o.gridx=0;
		o.gridy=1;
		panelOdtwarzanie.add(wyborZrodloB, o);
		
		o.gridx=0;
		o.gridy=2;
		
		panelOdtwarzanie.add(wyborZrodloA, o);
		
		o.gridheight = 2;
		o.weightx=0.33;
		o.gridx=2;
		o.gridy=0;
		panelOdtwarzanie.add(przyciskOdtwarzaj, o);
		o.gridheight = 2;
		o.gridx=3;
		o.gridy=0;
		
		panelOdtwarzanie.add(przyciskWstrzymaj, o);
		o.gridheight = 2;
		o.gridx=4;
		o.gridy=0;
		
		panelOdtwarzanie.add(przyciskZapisz, o);
		o.gridx=0;
		o.gridy=3;
		
		panelOdtwarzanie.add(przyciskWyciszenie, o);
		o.weightx=0;
		o.gridwidth = 3;
		o.gridx=2;
		o.gridy=3;
		panelOdtwarzanie.add(suwakGlosnosci, o );
		
		o.gridx=2;
		o.gridy=2;
		panelOdtwarzanie.add(przelacznikZapetlenia, o);
	   
    }
    
    /** Tworzenie wszytkich elementów niezbędnych do obsługi dzwięku **/
    public void ladowanieObslugiDzwieku() {
    	dA = new DzwiekSinus(440);
		dB = new DzwiekSinus(441);
    	dC = new DzwiekZlozenie();
		dC.setSkladowe(dA, dB);
		dC.przelicz();
		
		generator = new Generator(10);
		generator.setDzwiek(dC);
		generator.przelicz();
		
		rysunekA.podlaczDzwiek(dA);
		rysunekB.podlaczDzwiek(dB);
		rysunekC.podlaczDzwiek(dC);
	//	osCzasu.podlaczDzwiek(dC);
		
		odtwarzacz = new Odtwarzacz(generator, 20);
		
		malarz = new Malarz(configCzęstotliwoscAnimacji, rysunekA, rysunekB, rysunekC, odtwarzacz); //usuniecie osczasu z metody
    }
    
    
    /** Podpinanie sterownika **/
    public void podpiecieSterownika() {
    	sterownik = new Sterownik(this);
 	    przyciskOdtwarzaj.addActionListener(sterownik);
 	    przyciskWstrzymaj.addActionListener(sterownik);
 	    przyciskZapisz.addActionListener(sterownik);
 	    odtwarzacz.podepnijSterownik(sterownik);
 	    suwakCzestotliwoscA.addChangeListener(sterownik);
 	    suwakCzestotliwoscB.addChangeListener(sterownik);
 	    poleTekstoweCzestotliwoscA.addActionListener(sterownik);
 	    poleTekstoweCzestotliwoscB.addActionListener(sterownik);
 	    wyborZrodloA.addActionListener(sterownik);
 	    wyborZrodloB.addActionListener(sterownik);
 	    wyborZrodloAB.addActionListener(sterownik);
 	    przelacznikZapetlenia.addActionListener(sterownik);
 	    suwakGlosnosci.addChangeListener(sterownik);
 	    przyciskWyciszenie.addActionListener(sterownik);
 	    listaCzestotliwoscA.addActionListener(sterownik);
 	    listaCzestotliwoscB.addActionListener(sterownik);
 	    this.addWindowListener(sterownik);
		this.addComponentListener(sterownik);
    }
    
	public Fala() throws HeadlessException {
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setTitle("Fala");
		this.setLocation(50, 50);
		this.setMinimumSize(new Dimension(800, 600));;
		
		this.initPaneli(true); // parameter logiczny włączający wielobarne podświetlenie paneli
		this.ladowaniePaneluLoga();
		this.ladowaniePaneliOpcjiWejsc();
		this.ladowaniePaneluOdtwarzania();
		
		this.ladowanieObslugiDzwieku();
	   
	    this.pack();
	    
	    this.podpiecieSterownika();
	}

	public void dobierzCzasyRysunkowAB() {
		float czasAB = Math.max(dA.getT(), dB.getT())*4; 
		malarz.wA.ustawCzas(czasAB); 
		malarz.wB.ustawCzas(czasAB); 
	}
	
	public void uruchom() {
		setVisible(true);

		/** Pierwsze narysoanie wykresów. **/
		//osCzasu.ustawCzas(configRysowanyCzasC);
		rysunekC.ustawCzas(configRysowanyCzasC);
		dobierzCzasyRysunkowAB(); 
		malarz.zaplanuj(
				Malarz.PrzerysujA | Malarz.PrzerysujB | Malarz.PrzerysujC | Malarz.PrzerysujOs | 
				Malarz.OdswiezA |Malarz.OdswiezB |Malarz.OdswiezC | Malarz.OdswiezOs
		);

		/** Nadanie początkowych wartości kontrolkom częstotliowści. **/
		poleTekstoweCzestotliwoscA.ustawHz(dA.getF());
		listaCzestotliwoscA.ustawHz(dA.getF());
		suwakCzestotliwoscA.ustawHz(dA.getF());
		poleTekstoweCzestotliwoscB.ustawHz(dB.getF());
		listaCzestotliwoscB.ustawHz(dB.getF());
		suwakCzestotliwoscB.ustawHz(dB.getF());
		
		/** Wyświetlenie początkowych wartości na panelu wyników. **/
		panelWyniki.ustawCzestosci(dC.getCzestoscMala(), dC.getCzestoscDuza());
		
		/** Uruchminie wątków. **/
		ExecutorService exec = Executors.newFixedThreadPool(2);
		exec.execute(odtwarzacz);
		exec.execute(malarz);
		exec.shutdown();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(
			new Runnable(){
				public void run() {
					Fala okno = new Fala();
					okno.uruchom();
				}
			}
		);

		
	}


}
