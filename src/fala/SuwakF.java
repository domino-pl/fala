package fala;

import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class SuwakF extends JSlider {
	double scale;
	double minp, maxp, minv, maxv;
	
	public SuwakF(float minF, float maxF) {
		super();

		minp = 1;  /** Minimalna wartość całkowita suwaka. **/
		maxp = 2000;  /** Maksymalna wartość całkowita suwaka. **/
		minv = Math.log10(minF);  /** Logarytm minimalne wartości reprezentowanej. **/ 
		maxv = Math.log10(maxF); /** Logarytm maksymalnej wartości reprezentowanej. **/

		scale = (maxv-minv) / (maxp-minp); /** Wspólczynnik skalowania. **/
		
		this.setMinimum((int)minp);
		this.setMaximum((int)maxp);
	}

	/** Zwraca wartość ustawiną na suwaku w Hz. **/
	public float pobierzHz() {
		return  (float) (Math.round(Math.pow(10, minv + scale*(this.getValue()-minp))*10)/10.0);
	}
	
	/** Ustawia wartość na suwaku w Hz. **/
	public synchronized void ustawHz(float hz) {
		SwingUtilities.invokeLater(
			new Runnable(){
				public void run() {
					ChangeListener changeListener = getChangeListeners()[0];
					removeChangeListener(changeListener);
					setValue((int) Math.round((Math.log10(hz)-minv)/scale+minp));
					addChangeListener(changeListener);
				}
			}
		);
	}
}
