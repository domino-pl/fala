package fala;

public class DzwiekSinus implements Dzwiek {
	/** Klasa reprezentująca podstawowy dzięk (sinus). **/
	
	float f;  /** Częstotliwość **/
	float w;  /** Częstość **/
	
	/** Konstruktor od początkowej częstotliwości. **/
	public DzwiekSinus(float f) {
		super();
		this.setF(f);
	}

	/** Ustawia częstotliwość oraz automatycznie obliczoną częstość. **/
	public void setF(float f) {
		this.f = f;
		this.w = (float)(2 * Math.PI * this.f);
	}
	
	/** Zwraca aktualną częstotliwość. **/
	public float getF() {
		return this.f;
	}
	
	/** Zwaraca aktualną częstość. **/
	public float getW() {
		return this.w;
	}
	
	@Override
	public float get(float t) {
		return (float) Math.sin(t*this.w);
	}

	@Override
	public float getT() {
		return 1/this.f;
	}
}
