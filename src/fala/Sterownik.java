package fala;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/** Klasa realizująca logikę sterowania programu. **/
public class Sterownik implements ChangeListener, ActionListener, WindowListener, LineListener, ComponentListener {
	/** Referencja na instancję programu - wołamy składowe bezpośrednio. **/
	Fala fala;
	
	/** Polecenia przeliczenia poszczególnych wykresów i buforowanego dzwięku. **/
	boolean aktualizujA;
	boolean aktualizujB;
	boolean aktualizujC;
	boolean aktualizujGenerator;
	
	/** Konstruktor **/
	public Sterownik(Fala fala) {
		this.fala = fala;
	}
	
	/** Odpowiedzi na zdarzenia przycisków. **/
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		
		/** Naciśnięci przycisku odtwarzanie. **/
		if(source == fala.przyciskOdtwarzaj) {
			fala.odtwarzacz.odtwarzaj();
		}
		/** Naciśnięci przycisku wstrzymania. **/
		if(source == fala.przyciskWstrzymaj) {
			fala.odtwarzacz.zapauzuj();
		}
		
		/** 
		 * Wprowadzenie wanowej wartości do pól tekstowych częstotliwości A i B.
		 * Wewnątrz walidacja. 
		**/
		if(source == fala.poleTekstoweCzestotliwoscA) {
			float noweHz = fala.poleTekstoweCzestotliwoscA.pobierzHz();
			if(noweHz == -1) {
				fala.poleTekstoweCzestotliwoscA.ustawHz(fala.dA.getF());
			}else {
				fala.dA.setF(noweHz);
				aktualizujA = true;
			}
		}
		if(source == fala.poleTekstoweCzestotliwoscB) {
			float noweHz = fala.poleTekstoweCzestotliwoscB.pobierzHz();
			if(noweHz == -1) {
				fala.poleTekstoweCzestotliwoscA.ustawHz(fala.dB.getF());
			}else {
				fala.dB.setF(noweHz);
				aktualizujB = true;
			}
		}

		/** Rakcja na zmianę wyboru fali źródłowej. **/
		if(source == fala.wyborZrodloA) {
			if(fala.wyborZrodloA.isSelected()) {
				fala.generator.setDzwiek(fala.dA);
				aktualizujGenerator = true;
			}
		}
		if(source == fala.wyborZrodloB) {
			if(fala.wyborZrodloB.isSelected()) {
				fala.generator.setDzwiek(fala.dB);
				aktualizujGenerator = true;
			}
		}
		if(source == fala.wyborZrodloAB) {
			if(fala.wyborZrodloAB.isSelected()) {
				fala.generator.setDzwiek(fala.dC);
				aktualizujGenerator = true;
			}
		}
		
		/** Reakcja na wybranie predefiniowanej częstotliowści z lisata A lub B. **/
		if(source == fala.listaCzestotliwoscA) {
			float noweHz = fala.listaCzestotliwoscA.pobierzHz();
			if(noweHz == -1) {
				fala.listaCzestotliwoscA.ustawHz(fala.dA.getF());
			}else {
				fala.dA.setF(noweHz);
				aktualizujA = true;
			}
		}		
		if(source == fala.listaCzestotliwoscB) {
			float noweHz = fala.listaCzestotliwoscB.pobierzHz();
			if(noweHz == -1) {
				fala.listaCzestotliwoscB.ustawHz(fala.dB.getF());
			}else {
				fala.dB.setF(noweHz);
				aktualizujB = true;
			}
		}

		/** Reakcja na wciśnięcie przyciksu zatrzymania. **/
		if(source == fala.przyciskWyciszenie) {
			if(fala.przyciskWyciszenie.isSelected()) {
				try {
					BufferedImage icon;
					icon = ImageIO.read( ClassLoader.getSystemResource( "img/wyciszenie.png" ) );
					fala.przyciskWyciszenie.setIcon(new ImageIcon(icon));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}else {
				try {
					BufferedImage icon;
					icon = ImageIO.read( ClassLoader.getSystemResource( "img/glosnosc.png" ) );
					fala.przyciskWyciszenie.setIcon(new ImageIcon(icon));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			aktualizujGenerator = true;
		}
		
		/** Reakcja na kliknięcie pola zapętlenia. **/
		if(source == fala.przelacznikZapetlenia) {
			fala.odtwarzacz.zapetlenie(fala.przelacznikZapetlenia.isSelected());
		}
		
		/** Zapis do pliku. **/
		if(source == fala.przyciskZapisz) {
			File file = null;
			boolean abort = false;
			
			JFileChooser fc = new JFileChooser();
			fc.setSelectedFile(new File("fala.wav"));  /** Wybór domyślnej nazwy pliku. **/
			int returnVal = fc.showSaveDialog(fala);
			
			if(returnVal == JFileChooser.APPROVE_OPTION)
				file = fc.getSelectedFile();
			else
				abort = true;
			
			/* Ostrzeżerzenie o nadpisaniu istniejącego pliku. */
			if(!abort && file.exists()){
				Object[] options = {"Tak", "Nie"};

				int n = JOptionPane.showOptionDialog(
					null,
					"Czy napewno nadpisać "+file.getAbsolutePath()+"?",
		    		"Ostrzerzenie",
				    JOptionPane.YES_NO_OPTION,
				    JOptionPane.QUESTION_MESSAGE,
				    null,
				    options,
				    options[1]
				 );
				if(n != JOptionPane.YES_OPTION)
					abort = true;
			}
			
			/** Zapisyawnie. **/
			if(!abort) {
				try {
					fala.generator.zapisz(file, fala.configZapisDlugosc);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(
							fala,
						    "Błąd zapisywania",
						    "Błąd",
						    JOptionPane.ERROR_MESSAGE
					);
					e.printStackTrace();
				}
			}
				
		}
		
		aktualizuj();
	}

	/** Obsługa zmian pozycji suwaków. **/
	@Override
	public void stateChanged(ChangeEvent arg0) {
		Object source = arg0.getSource();
		
		/** Obsługa zmian pozycji suwaków częstotliwości A i B. **/
		if(source == fala.suwakCzestotliwoscA) {
			fala.dA.setF(fala.suwakCzestotliwoscA.pobierzHz());
			aktualizujA = true;
		}
		if(source == fala.suwakCzestotliwoscB) {
			fala.dB.setF(fala.suwakCzestotliwoscB.pobierzHz());
			aktualizujB = true;
		}
		
		/** Obsługa zmian pozycji suwaka głośności. **/
		if(source == fala.suwakGlosnosci) {
			aktualizujGenerator = true;
		}
		
		aktualizuj();
	}

	@Override
	public void windowActivated(WindowEvent arg0) {}

	/** Obsługa zamykania programu. **/
	@Override
	public void windowClosed(WindowEvent arg0) {
		fala.odtwarzacz.zakoncz();
		fala.malarz.zakoncz();
	}

	@Override
	public void windowClosing(WindowEvent arg0) {}
	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	@Override
	public void windowOpened(WindowEvent arg0) {}

	/** Aktualizowanie parametrów dzwienków i zawartości bufora generatora. **/
	protected void aktualizuj() {
		if(aktualizujA) {
			fala.dobierzCzasyRysunkowAB();
			fala.malarz.zaplanuj(Malarz.PrzerysujA | Malarz.OdswiezA | Malarz.PrzerysujB | Malarz.OdswiezB);
			fala.poleTekstoweCzestotliwoscA.ustawHz(fala.dA.getF());
			fala.listaCzestotliwoscA.ustawHz(fala.dA.getF());
			fala.suwakCzestotliwoscA.ustawHz(fala.dA.getF());
			
			aktualizujC = true;
			if(fala.generator.getDzwiek() == fala.dA)
				aktualizujGenerator = true;
		}
	
		if(aktualizujB) {
			fala.dobierzCzasyRysunkowAB();
			fala.malarz.zaplanuj(Malarz.PrzerysujA | Malarz.OdswiezA | Malarz.PrzerysujB | Malarz.OdswiezB);
			fala.poleTekstoweCzestotliwoscB.ustawHz(fala.dB.getF());
			fala.listaCzestotliwoscB.ustawHz(fala.dB.getF());
			fala.suwakCzestotliwoscB.ustawHz(fala.dB.getF());
			
			aktualizujC = true;
			if(fala.generator.getDzwiek() == fala.dB)
				aktualizujGenerator = true;
		}
		
		if(aktualizujC) {
			fala.dC.przelicz();
			fala.malarz.zaplanuj(Malarz.PrzerysujC | Malarz.OdswiezC);
			
			fala.panelWyniki.ustawCzestosci(fala.dC.getCzestoscMala(), fala.dC.getCzestoscDuza());
			
			if(fala.generator.getDzwiek() == fala.dC)
				aktualizujGenerator = true;
		}
		
		if(aktualizujGenerator) {
			fala.generator.setGlosnosc(
				fala.przyciskWyciszenie.isSelected() ? 0 : fala.suwakGlosnosci.getValue()
			);
			fala.generator.przelicz();
		}
			
		aktualizujA = aktualizujB = aktualizujC = aktualizujGenerator = false;
	}

	/** Obsługa zdarzeń odtwarzacza. **/
	@Override
	public void update(LineEvent arg0) {
		if(arg0.getSource() == fala.odtwarzacz.getSourceDataLine()) {
			if(arg0.getType() == LineEvent.Type.START) {
				fala.przyciskWstrzymaj.setEnabled(true);
				fala.przyciskOdtwarzaj.setEnabled(false);
			}
			
			if(arg0.getType() == LineEvent.Type.STOP) {
				fala.przyciskWstrzymaj.setEnabled(false);
				fala.przyciskOdtwarzaj.setEnabled(true);
			}
		}
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {}
	@Override
	public void componentMoved(ComponentEvent arg0) {}

	/** Obsługa zmiany rozmiaru okna. **/
	@Override
	public void componentResized(ComponentEvent arg0) {
		if(arg0.getSource() == fala) {
			fala.malarz.zaplanuj(
					Malarz.PrzerysujOs | Malarz.OdswiezOs
					| Malarz.PrzerysujA | Malarz.OdswiezA
					| Malarz.PrzerysujB | Malarz.OdswiezB
					| Malarz.PrzerysujC | Malarz.OdswiezC
			);
		}
		
	}

	@Override
	public void componentShown(ComponentEvent arg0) {}
	
}
