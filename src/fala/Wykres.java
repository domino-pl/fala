package fala;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class Wykres extends JPanel {
	Dzwiek dzwiek = null;  /** Dzwięk do wyświetlenie. **/
	
	BufferedImage wykres = null;  /** Bufor reprezentujący aktualny wykres funkcji. **/
	BufferedImage wykresZeWskaznikiem = null;  /** Bufor reprezentujący aktualny wykres funkcji z naniesionym wskaźnikiem. **/
	BufferedImage rysowanyBuffor = null;  /** Bufor do narysowania (referencja na wykres ze wskaźnikiem lub bez). **/
	
	float wskaznik = -1;  /** Pozycja wskaźnika - -1 oznacza wyłączony. **/
	float czas = 1;  /** Okres czasu jaki przedstawia wykres. **/
	
	float skalaX; /** Skala osi x: sekunda na piksel **/
	
	/** Funkcie kopujace płutna. **/
	private void kopiuj(BufferedImage zrodlo, Graphics2D cel) {
		cel.drawImage(zrodlo, 0, 0, this);
	}
	private void kopiuj(BufferedImage zrodlo, Graphics cel) {
		cel.drawImage(zrodlo, 0, 0, this);
	}
	
	/** Konstruktor **/
	public Wykres() {
		super();
	}
	
	/** Podpina dzwięk źródłowy. **/
	public void podlaczDzwiek(Dzwiek dzwiek) {
		/** Metoda podłączająca funkcię do narysowania **/
		this.dzwiek = dzwiek;
	}
	
	/** Ustawia jaki okres czasu w sekundach ma być rysowany. **/
	public synchronized void ustawCzas(float czas) {
		this.czas = czas;
	}
	
	/** Ustawia wskaźnik (-1 wyłącza) **/
	public synchronized void ustawWskaznik(float wskaznik) {
		this.wskaznik = wskaznik;
	}
	
	/** Rusuje wykres. Do wywołania po zmianie rozmiaru okna lub cech fali. Na koniec nanosi wskaźnik. **/
	public void przerysuj() {
		/** Pobranie informacji jaki okres czasu ma być ruysowany. **/
		float czas;
		synchronized (this) {
			czas = this.czas;
		}
		
		/** Określenia skali osi x. **/
		this.skalaX = this.getWidth()/czas;
		
		/** Przygotowywaie obszaru rysowania. **/
		this.wykres =  new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) this.wykres.getGraphics();
		g2.setColor(Color.BLACK);
		g2.setStroke(new BasicStroke(1));
		/** Naniesienie osi x. **/
		g2.drawLine(0, this.getHeight()/2, this.getWidth(), this.getHeight()/2); // os X
		
		/** Rysowanie wykresu. **/
		int stareX = 0;
		int stareY = 0;
		int noweX, noweY;
		float krok = czas/1000;
		for(float t = 0; t <= czas; t+=krok) {
			noweX = (int)(this.skalaX*t);
			noweY = this.getHeight()/2 - (int)((this.getHeight()/2-10)* this.dzwiek.get(t));
			
			g2.drawLine(stareX, stareY, noweX, noweY);
			
			stareX = noweX;
			stareY = noweY;
		}

		/** Nanoszenie wskaźnika. **/
		this.narysujWskaznik();
	}
	
	/** Nanoszenie wskaźnika na wykres. Jeżeli wskaźnik wyłączony kieruje refenecję na czysty wykres. **/
	public void narysujWskaznik() { 
		/** Pobranie aktualniej pozycji wskaźnika. **/
		float wskaznik;
		synchronized (this) {
			wskaznik = this.wskaznik;
		}
		
		/** Obsłużenie przypadku wyłączonego wskaźnika. **/
		if(wskaznik == -1) {
			this.rysowanyBuffor = this.wykres;
			return;
		}
		
		/** Przygotowanie kopii na podstawie czystego wykresu. **/
		this.wykresZeWskaznikiem =  new BufferedImage(wykres.getWidth(), wykres.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) wykresZeWskaznikiem.getGraphics();
		kopiuj(wykres, g2);
		
		/** Naniesienie wskaźnika **/
		g2.setColor(Color.RED);
		g2.setStroke(new BasicStroke(2));
		int pozycja = (int)(skalaX*wskaznik) % this.getWidth();
		g2.drawLine(pozycja,0,pozycja,this.getHeight());  //rysowanie wskaznika obecnej sekundy dzwieku
		synchronized (this) {
			this.rysowanyBuffor = this.wykresZeWskaznikiem;
		}
	}
	
	@Override
	public synchronized void paint(Graphics g) {
		super.paint(g);

		/** Rysownie bufora na panelu. **/
		synchronized (this) {
			if(rysowanyBuffor != null){
				kopiuj(rysowanyBuffor, g);
			}
		}
	}
	
}
