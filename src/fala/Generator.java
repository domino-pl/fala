package fala;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

public class Generator {
	final int probkowanie = 44100;
	final int czasBufora;  /** Rozmiar bufora w sekundach **/
	
	AtomicInteger glosnosc;  /** Głośność **/
	
	byte bufor[] = null;  /** Bufor zawierający wygenerowany dzwięk **/
	int rozmiarBufora = 0;	/** Rozmiar bufora w bajtach **/
	
	Dzwiek dzwiek;  /** Dzwięk źródłowy **/
	final AudioFormat format;
	
	/** Konstruktor od czasu trzymanego w buforze w sekundach **/
	public Generator(int czasBufora) {
		this.czasBufora = czasBufora;
		
		glosnosc = new AtomicInteger(100);
		format = new AudioFormat(probkowanie, 8, 1, true, false);
		rozmiarBufora = probkowanie*czasBufora;
		bufor = new byte[rozmiarBufora];
	}

	/** Zwraca używany format. **/
	public AudioFormat getFormat() {
		return format;
	}
	
	/** Zwraca próbkowanie. **/
	public int getProbkowanie() {
		return probkowanie;
	}
	
	/** Ustawnia głośność z zakresu 0-100. **/
	public void setGlosnosc(int glosnosc) {
		this.glosnosc.set(glosnosc);
	}
	
	/** Ustatwia dzwięk źródłowy. **/
	public synchronized void setDzwiek(Dzwiek dzwiek) {
		this.dzwiek = dzwiek;
	}
	
	/** Zwraca dzwięk źródłowy. **/
	public Dzwiek getDzwiek() {
		return this.dzwiek;
	}
	
	/** Przelicza zawartość buffra (do wykoninaia po zmianie głośności lub dzwięku źródłowego). **/
	public synchronized void przelicz() {
		for(int i=0; i<rozmiarBufora; i++) {
			bufor[i] = (byte) (dzwiek.get((float) i / probkowanie) * glosnosc.get() );
		}
	}
	
	/** Zwraca rozmiar bufora. **/
	public int getRozmiarBufora() {
		return this.rozmiarBufora;
	}

	/** Zwraca bufor - wykorzystywane w odtwarzaczu. **/
	public synchronized byte[] getBuffor() {
		return this.bufor;
	}
	
	public void zapisz(File cel, int dlugosc) throws IOException {
		/** Bufor z danymi do zapisania w pliku. **/
		byte buf[] = new byte[probkowanie*dlugosc];
		for(int i=0; i<probkowanie*dlugosc; i++) {
			buf[i] = (byte) (dzwiek.get((float) i / probkowanie) * glosnosc.get() );
		}
		
		ByteArrayInputStream bais = new ByteArrayInputStream(buf);
		AudioInputStream ais = new AudioInputStream(bais, format, probkowanie*dlugosc);
		AudioSystem.write(ais, AudioFileFormat.Type.WAVE, cel);
	}
}
