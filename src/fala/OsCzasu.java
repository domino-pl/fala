package fala;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/** Klasa reprezentująca panel osi czasu, dziedzicząca z kalsy wykresu dzwięku, jednak nie implementująca rysowania samego wykresu. **/
@SuppressWarnings("serial")
public class OsCzasu extends Wykres {
	/** Konstruktor **/
	public OsCzasu() {
		super();
	}
	
	/** Rysowanie **/
	public void przerysuj() {
		/** Pobranie informacji jaki okres czasu ma być ruysowany. **/
		float czas;
		synchronized (this) {
			czas = this.czas;
		}
		
		/** Określenia skali osi x. **/
		this.skalaX = this.getWidth()/czas;
		
		/** Przygotowywaie obszaru rysowania. **/
		this.wykres =  new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) this.wykres.getGraphics();
		g2.setColor(Color.BLACK);
		g2.setStroke(new BasicStroke(1));
		/** Naniesienie osi x. **/
		
		// TODO: można skasować prostą albo ją przenieść  i trzeba dorobić jakieś etykiety
		g2.drawLine(0, this.getHeight()/2, this.getWidth(), this.getHeight()/2); // os X
		
		
		
		
		/** Nanoszenie wskaźnika. **/
		this.narysujWskaznik();
	}
}
