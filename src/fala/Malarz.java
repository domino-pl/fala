package fala;

import java.util.concurrent.atomic.AtomicBoolean;

/** Wątek realizujący animacje, w tym przeliczenia wykresów. **/
public class Malarz implements Runnable {
	/** Flagi poleceń przerysowania poszczególnych osi i wykresów. **/
	final static int PrzerysujOs = 1<<0;
	final static int PrzerysujA = 1<<1;
	final static int PrzerysujB = 1<<2;
	final static int PrzerysujC = 1<<3;
	/** Flagi poleceń naniesienia wyskaźnika na poszczególne osi i wykresy. **/
	final static int WskazOs = 1<<4;
	final static int WskazA = 1<<5;
	final static int WskazB = 1<<6;
	final static int WskazC = 1<<7;
	/** Flagi poleceń odświerzenia wyświetlania poszczególnych osi i wykresó. **/
	final static int OdswiezOs = 1<<8;
	final static int OdswiezA = 1<<9;
	final static int OdswiezB = 1<<10;
	final static int OdswiezC = 1<<11;
	
	/** Zmienna reprezentująca zadane polecenia. **/
	int zaplanowane = 0;
	
	/** Flaga nakazująca natychmiastowe zakończenie wątka. **/
	AtomicBoolean flagaZakonczenia;
	
	int animiationSleep;
	
	/** Refetencje na wykresy i odtwarzacz. **/
	//OsCzasu os;
	Wykres wA, wB, wC;
	Odtwarzacz odtwarzacz;
	
	/** Dodaje zadanie. **/
	public synchronized void zaplanuj(int zadania) {
		zaplanowane += zadania;
	}
	
	/** Konstruktor **/
	public Malarz(int animationFrequency, Wykres wA, Wykres wB, Wykres wC, Odtwarzacz odtwarzacz) { // usuniecie osczasu
		flagaZakonczenia = new AtomicBoolean(false);
		
		this.animiationSleep = 1000/animationFrequency;
		//this.os = os;
		this.wA = wA;
		this.wB = wB;
		this.wC = wC;
		this.odtwarzacz = odtwarzacz;
	}

	/** Wydaje polecenie natychmiastowego zakończenia. **/
	public void zakoncz() {
		synchronized(this) {
			flagaZakonczenia.set(true);
			this.notifyAll();
		}
	}
	
	@Override
	public void run() {
		int zrob;
		float pozycjaOdtwarzania;
		
		while(!flagaZakonczenia.get()) {	
			/** Ustawnie wskaźnika na wykresie funkcji C **/
			pozycjaOdtwarzania = odtwarzacz.pozycja();
			if(pozycjaOdtwarzania != -1) {
				wC.ustawWskaznik(pozycjaOdtwarzania);
			//	os.ustawWskaznik(pozycjaOdtwarzania);
				zaplanuj(WskazOs|OdswiezOs|WskazC|OdswiezC);
			}
			
			/** Pobranie listy zadań. **/
			synchronized (this) {
				zrob = zaplanowane;
				zaplanowane  = 0;
			}
			
			/** Realizacja zadań **/
			//if((zrob & PrzerysujOs) > 0)
			//	os.przerysuj();
			if((zrob & PrzerysujA) > 0)
				wA.przerysuj();
			if((zrob & PrzerysujB) > 0)
				wB.przerysuj();
			if((zrob & PrzerysujC) > 0)
				wC.przerysuj();
			//if((zrob & WskazOs) > 0)
			//	os.narysujWskaznik();
			if((zrob & WskazA) > 0)
				wA.narysujWskaznik();
			if((zrob & WskazB) > 0)
				wB.narysujWskaznik();
			if((zrob & WskazC) > 0)
				wC.narysujWskaznik();
			//if((zrob & OdswiezOs) > 0)
			//	os.repaint();
			if((zrob & OdswiezA) > 0)
				wA.repaint();
			if((zrob & OdswiezB) > 0)
				wB.repaint();
			if((zrob & OdswiezC) > 0)
				wC.repaint();

			try {
				Thread.sleep(animiationSleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
