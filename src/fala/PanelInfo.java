package fala;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

/** Klasa reprezentująca panel wyświetlający wyniki. **/
@SuppressWarnings("serial")
public class PanelInfo extends JPanel  {
	
	JLabel etykietaCzestoscMala;
	JLabel etykietaCzestoscDuza;
	JLabel etykietaOkresMaly;
	JLabel etykietaOkresDuzy; /**zadeklarowanie etykiet**/
	
	
	public PanelInfo() {
		etykietaCzestoscMala = new JLabel();
		etykietaCzestoscDuza = new JLabel();
		etykietaOkresMaly = new JLabel();
		etykietaOkresDuzy = new JLabel(); /**stworzenie etykiet**/
		
		this.setBorder(BorderFactory.createLineBorder(Color.black, 4, true));
		this.setLayout(new GridLayout(2,2)); /** ustawienie layoutu **/
		
		this.add(etykietaCzestoscMala);
		this.add(etykietaCzestoscDuza);
		this.add(etykietaOkresMaly);
		this.add(etykietaOkresDuzy);  /** dodanie kolejnych elementow do panelu **/

	}
	
	
	/** Funkcja ustawiąjąca częstości do wyświetlenia **/
	public void ustawCzestosci(float mala, float duza){
		etykietaCzestoscMala.setText(" ω mała: " + String.format("%.2g%n", 2*Math.PI/mala) + "  ");
		etykietaCzestoscDuza.setText("ω duża: " + String.format("%.2g%n", 2*Math.PI/duza) + "  ");
		etykietaOkresMaly.setText(" T mały: " + String.format("%.2g%n", 1/mala) + "  ");
		etykietaOkresDuzy.setText("T duży: " + String.format("%.2g%n", 1/duza) + "  ");
		
	}
}
 