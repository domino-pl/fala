package fala;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/** Klasa implementująca wątek odtwarzający dzwięk. **/
public class Odtwarzacz implements Runnable {
	Generator generator;  /** Referencja na generator zawierający bufor z dzwiękiem do odtworzenia. **/
	
	AtomicBoolean flagaPauza;  /** Flaga informująca o konieczności zapauzowania odtwarzania. **/
	AtomicBoolean flagaZakonczenia;  /** Flaga informująca o konieczności zakończenia wątka. **/
	AtomicBoolean flagaZapetlenia;  /** Flaga informująca o konieczności zapętlenia. **/
	
	long czasOdtwarzaniaMs;  /** Czas odtwarzania dzwięku przy wyłączonym zapętleniu. **/
	long startCzasMs = -1;
	
	SourceDataLine sdl;  /** Obiekt reprezentujący wyjście dzwiękowe. **/
	
	
	/** 
	 * Konstuktor ze wsazaniem źródłowego generatora dzwięku i 
	 * czasu odtwarzania przy wyłączonym zapętleniu w sekundach. 
	**/
	public Odtwarzacz(Generator generator, float czasOdtwarzania) {
		this.generator = generator;
		this.czasOdtwarzaniaMs = (long) (czasOdtwarzania * 1000000);
		
		flagaPauza = new AtomicBoolean(true);
		flagaZapetlenia = new AtomicBoolean(false);
		flagaZakonczenia = new AtomicBoolean(false);
		
		try {
			sdl = AudioSystem.getSourceDataLine(generator.getFormat());
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	/** Zwraca linię dzwięku - potrzebne w sterowniku do rozpoznania zródła zdarzenia. **/
	public SourceDataLine getSourceDataLine() {
		return sdl;
	}
	
	/** Podpina sterowink odbierający zdarzenia z lini dzwięku. **/
	public void podepnijSterownik(LineListener sterownik) {
		sdl.addLineListener(sterownik);
	}
	
	/** Nakazuje rozpoczęcie odtwarzania. **/
	public void odtwarzaj() {
		flagaPauza.set(false);
		
		synchronized (this) {
			this.notifyAll(); /** Obudzenie wątku jeżli aktualnie czeka **/
		}
	}
	
	/** Nakazuje zapauzowanie odtwarzania. **/
	public void zapauzuj() {
		flagaPauza.set(true);	
	}
	
	/** Włącza lub wyłącza zapętlenie. **/
	public void zapetlenie(boolean zapetlenie) {
		flagaZapetlenia.set(zapetlenie);	
	}
	
	/** Nakazuje natychmiastowe zatrzymanie odtwarzania i zakończenie wątku. **/
	public void zakoncz() {
		flagaZakonczenia.set(true);
		synchronized (this) {
			this.notifyAll(); /** Obudzenie wątku jeżli aktualnie czeka **/
		}
	}
	
	/** 
	 * Zwraca aktualną pozycję odtwarzania w sekundach.
	 * Używane do naniesienia wskaźnika.
	 * Jeżeli aktualnie odtwarzanie jest wyłączone zwraca -1.
	**/
	public float pozycja() {
		if(startCzasMs == -1 || flagaPauza.get())
			return -1;
		return (float) ((sdl.getMicrosecondPosition() - startCzasMs)/1000000.0);
	}
	
	@Override
	public void run() {
		try {
			/** 
			 * Otwarcie wyjścia dźwiękowego o formacie pobranym z generatora.
			 * Od drugiego parametru(rozmiaru bufora zależy dokładność wskaźnika
			 * ustawianego na podstawie informacji zwracanych przez metodę pozycja
			 * oraz szybkość aktualizacji bufora przy zmianie dzwięku - obecnie 
			 * rozmier bufora wynosi 1/6 sekundy.
			**/
			sdl.open(generator.getFormat(), generator.getProbkowanie() / 6);
			
			while(!flagaZakonczenia.get()) {
				synchronized(this) {
					this.wait(100);
				}
				
				/** Obsługa pauzy. **/
				if(flagaPauza.get())
					continue;
				
				/** Porcja danych o jakie jest uzupełniony bufor kiedy jest
				 * w nim dostatenie dużo miejsca. **/
				int dodawanie = sdl.getBufferSize()/3;
				
				int pozycjaWBuforze = 0;  /** Informacja o momencie w buforze generatora. **/
				startCzasMs = sdl.getMicrosecondPosition();  /** Referecyjny czas rozpoczęcia odtwarzania. **/
				long aktulnyCzasMs = 0;  /** Informacja ile ms zostało już odtworzone. **/
					
				sdl.start();  /** Rozpoczęcie odtwarzania dzwięku. **/
				
				/** Odtwarzanie dopóki nie zostanie przekroczony czasOdtwarzaniaMs i nie zostanie wyłączona flaga zapętlenia. **/
				while(aktulnyCzasMs < czasOdtwarzaniaMs || flagaZapetlenia.get()){
					/** Obsłużenie zapauzowaniania **/
					if(flagaPauza.get())
						break;
					
					/** Obsłużenie polecenia natychmiastowego zakończenia wątka. **/
					if(flagaZakonczenia.get())
						break;
					
					/** Dodanie danych z generatora do odtworzenia. **/
					pozycjaWBuforze += sdl.write(
							generator.getBuffor(), 
							pozycjaWBuforze, 
							pozycjaWBuforze+dodawanie <= generator.getRozmiarBufora() ? dodawanie : generator.getRozmiarBufora()-pozycjaWBuforze
					);
					/* Obliczanie nowych pozycji na podstawie ilości dodanych do odtowrzenia bajtów. **/
					pozycjaWBuforze %= generator.rozmiarBufora;
					aktulnyCzasMs = sdl.getMicrosecondPosition() - startCzasMs;
				}
				
				/** Zatrzymanie odtwarzania i oczyszczenie bufora. **/
				sdl.stop();
				sdl.flush();
				
				/** Zasgnalizowanie zapuzowania **/
				flagaPauza.set(true);
			}

			/** Zamknięcie wyjścia dźwiękowego **/
			sdl.close();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
