package fala;

public interface Dzwiek {
	/**
	 * Interfejs represzentujący dzwięk do wygenerowania/narysowania .
	 * Implementowany zarówno przez dzwięki podstawowe (czyste sinusy - DzwiekSinus)
	 * jak i złożenia DzwiękZlozenia.
	**/
	
	/** Zwraca wartość funkcji opiującej dzwięk w danej chwili czasu "t". **/
	public float get(float t);
	
	/** Zwraca okres funkcji opisującej dzwięk. **/
	public float getT();
}
