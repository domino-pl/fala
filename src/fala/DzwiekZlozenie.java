package fala;

public class DzwiekZlozenie implements Dzwiek {
	float wm = 1;  /** Częstość mniejsza **/
	float ww = 1;  /** Częstość większa **/
	
	DzwiekSinus dA = null;  /** Referencja na pierwszą falę składową. **/
	DzwiekSinus dB = null;  /** Referencja na drugą falę składową. **/
	
	/** Konstruktor **/
	public DzwiekZlozenie() {
		super();
	}

	/** Ustawia dwie składowe false dA i dB. **/
	public void setSkladowe(DzwiekSinus dA, DzwiekSinus dB) {
		this.dA = dA;
		this.dB = dB;
		
		this.przelicz(); // przeliczenie częstości
	}
	
	/** Oblicza częstości fali wyjściowej na podstawie informacji podanych z fal składowych. **/
	public void przelicz() {
		wm = ( dA.getW() - dB.getW() ) / 2;
		ww = ( dA.getW() + dB.getW() ) / 2;
	}
	
	@Override
	public float get(float t) {
		return (float) (Math.cos( wm * t ) * Math.sin( ww * t )); 
	}

	@Override
	public float getT() {
		/** Nie jest to właściwy okres funkcji, ale okres dudnienia. **/
		return (float) (2*Math.PI / wm);
	}
	
	public float getCzestoscDuza() {
		return Math.abs(ww);
	}
	
	public float getCzestoscMala() {
		return Math.abs(wm);
	}	
}
